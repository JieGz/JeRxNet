package com.jgz.jerxnet;

import android.app.Application;

import com.jgz.rxnet.RxNet;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by gdjie on 2017/6/14.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        RxNet.init("http://test.ns002.com/", HttpLoggingInterceptor.Level.BODY);
    }
}
