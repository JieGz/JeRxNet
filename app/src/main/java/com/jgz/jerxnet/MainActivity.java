package com.jgz.jerxnet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jgz.rxnet.HttpResult;
import com.jgz.rxnet.NetReqObserver;
import com.jgz.rxnet.RxNet;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        RxNet.newObserver(JeApi.Wrapper.getQueryQuote(), new NetReqObserver<HttpResult>() {
            @Override
            public void onSuccess(HttpResult httpResult) {
                System.out.println(httpResult);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxNet.release();
    }
}
