package com.jgz.jerxnet;

import com.jgz.rxnet.HttpResult;
import com.jgz.rxnet.RxNet;


import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by gdjie on 2017/6/14.
 */

public interface JeApi {
    @GET("/")
    Observable<HttpResult> getQueryQuote();

    class Wrapper{
        public static Observable<HttpResult> getQueryQuote(){
            return RxNet.getService(JeApi.class).getQueryQuote();
        }
    }
}
