package com.jgz.rxnet;


import android.util.Log;

import rx.Subscriber;

/**
 * 观察者抽象类
 * Created by Je on 2017/06/14.
 */
public abstract class NetReqObserver<T> extends Subscriber<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        try {
            if (e != null) {
                _onError(e);
            }
        } catch (Exception e1) {
            //e1.printStackTrace();
        }
    }

    @Override
    public void onNext(T t) {
        if (t == null) {
            _onError(new Error("获取网络数据失败"));
        } else {
            //还可以在这里进行onCompleted函数的调用
            onSuccess(t);
        }
    }

    public abstract void onSuccess(T t);

    public void _onError(Throwable e) {
        Log.e("网络请求", e.getMessage());
        e.printStackTrace();
    }
}
